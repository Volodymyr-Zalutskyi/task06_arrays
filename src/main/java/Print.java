import java.util.Set;

public class Print {
    public static void printArray(Set<Integer> array){
        for (Integer n: array) {
            System.out.print(n + " ");
        }
        System.out.println();
    }
    public static void printArray(int [] array){
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            if(i<(array.length-1))
            {
                System.out.print(",");
            }
            else
                System.out.print(".");
        }
        System.out.println();
    }

    public static void printArray(String [] array){
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            if(i<(array.length-1))
            {
                System.out.print(",");
            }
            else
                System.out.print(".");
        }
        System.out.println();
    }
}
