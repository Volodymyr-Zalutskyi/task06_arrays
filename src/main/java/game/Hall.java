package game;

import java.util.Arrays;

public class Hall {
    public static final int ROOM = 10;

    public Hero hero;
    public Enemy enemy;
    private int life;


    public Hall() {
        hero = new Hero();
        enemy = new Enemy();
    }

    public void setLife(int life) {
        this.life = life;
    }

    public String[] behindOfDoor() {
        String [] whatOrWho = new String[ROOM];
        int count = 0;
        for (int i = 0; i < ROOM; i++) {
            if (enemy.getIndex() == (MyRandom.random.nextInt(2)+1)){
                 whatOrWho[count++] = enemy.getName();
            }else {
                whatOrWho[count++] = "Magic";
            }
        }
        return whatOrWho;
    }

    public int[] strengthEnemy() {
       int [] strength = new int[ROOM];
       for (int i = 0; i < ROOM; i++) {
           enemy.setPower(MyRandom.random.nextInt(96)+5);
           strength[i] = enemy.getPower();
       }
       return strength;
    }
    public int [] getLife() {
        int [] getLife = new int[ROOM];
        for (int i = 0; i < ROOM; i++) {
            setLife(MyRandom.random.nextInt(81)+10);
            getLife[i] = life;
        }
        return getLife;
    }
}
