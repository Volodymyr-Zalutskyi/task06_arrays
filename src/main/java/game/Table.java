package game;

public class Table {
    public final static String line = "-----------------------------------------------------" + "\n";
    private static String[] behindOfDoor = new Hall().behindOfDoor();
    private static int[] joker = new Hall().strengthEnemy();
    private static int[] life = new Hall().getLife();
    private static int heroLife = new Hall().hero.getPower();
    private static String [] x = {"Room", "Behind the door", "Life", "Life hero"};
    private static int[] magicOrEnemy = enemyOrMagicLife();

    public static void printTable(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            if (i < (array.length - 1)) {
                System.out.print(",");
            } else
                System.out.print(".");
        }
        System.out.println();
    }

    public static void printTable(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            if (i < (array.length - 1)) {
                System.out.print(",");
            } else
                System.out.print(".");
        }
        System.out.println();
    }

    public static int[] enemyOrMagicLife() {
        int count = 0;
        int[] enemyOrLife = new int[Hall.ROOM];
        for (int i = 0; i < Hall.ROOM; i++) {
            if (behindOfDoor[i].equals("Magic")) {
                enemyOrLife[i] = joker[i];
            }
            if (behindOfDoor[i].equals(new Hall().enemy.getName())) {
                enemyOrLife[i] = life[i];
            }
        }
        return enemyOrLife;
    }

    public static void printTable() {
        System.out.print(line);
        System.out.println("| Room\t| Behind the door\t| Life\t\t| Life hero |\t");
        System.out.print(line);
        for (int j = 0; j < Hall.ROOM; j++) {
            System.out.print("|"+ (1+j) + "\t"+"\t"+"|");
            System.out.print("\t" + behindOfDoor[j] + "\t"+"\t"+"\t"+"|");
            System.out.print("\t" + magicOrEnemy[j] + "\t"+"\t"  + "|");
            System.out.print("\t" + heroLife + "\t" +"\t"+"|" + "\n");
        }
        System.out.print(line);
    }
}
