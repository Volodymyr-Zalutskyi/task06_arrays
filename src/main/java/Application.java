import game.Hall;
import game.MyRandom;

import java.util.Set;


public class Application {
    public static void main(String[] args) {
        int[] array1 = {1, 1, 1, 2, 3, 4, 5};
        int[] array2 = {1, 3, 5, 6, 9, 11};
        int[] arr = Exercise.mergeArray(array1, array2);

        Print.printArray(arr);
        int[] newArr = Exercise.noDuplicate(arr);
        Print.printArray(newArr);

//        Set<Integer> arrSet = Exercise.noDuplicateSet(arr);
//        Print.printArray(arrSet);

        Deque a = new Deque(20);
        for (int f = 1; f < 20; f += 2) {
            a.addFront(a, f);
            a.addBack(a, f + 1);
        }
        System.out.print("(");
        for (int i = 0; i < 20; i++) {
            System.out.print(a.removeFront(a));
            if (i < 19)
                System.out.print(",");
        }
        System.out.print(")");

    }

}
