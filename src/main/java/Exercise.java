import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Exercise {

    public static int[] mergeArray(int[] array1, int[] array2) {
        int first = array1.length;
        int second = array2.length;
        int[] array3 = new int[first + second];
        int j = 0;
        for (int i = 0; i < first; i++) {
            array3[j] = array1[i];
            j++;
        }
        for (int i = 0; i < second; i++) {
            array3[j] = array2[i];
            j++;
        }
        return array3;
    }

    public static int[] noDuplicate(int[] array) {
        insertSort(array);
        int[] newArray;
        int dup = 0;
        int length = array.length;
        for (int i = 1; i < length; i++) {
            if (array[i] == array[i - 1]) {
                dup++;
            }
        }
        newArray = new int[length - dup];
        int index = 1;
        newArray[0] = array[0];
        for (int j = 1; j < length; j++) {
            if (array[j] != array[j - 1]) {
                newArray[index] = array[j];
                index++;
            }
        }
        return newArray;
    }

    public static Set<Integer> noDuplicateSet(int [] arr) {
        Set<Integer> result = new TreeSet<Integer>();
        for (int i = 0; i < arr.length; i++) {
            result.add(arr[i]);
        }
        return result;
    }

    private static void bubbleSort(int[] array) {
        int length = array.length;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    private static int[] insertSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int current = array[i];
            int j = i;
            while (j > 0 && array[j - 1] > current) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = current;
        }
        return array;
    }
}
